package main

import (
	"crypto/tls"
	"github.com/labstack/gommon/log"
	"hikvision/artemis"
	"net/http"
	"os"
	"time"
)

func main() {
	args := os.Args[1:]
	if len(args) < 2 {
		log.Info("Usage: go run main.go (API_KEY) (API_SECRET)")
		os.Exit(0)
	}
	apiKey, apiSecret := args[0], args[1]
	httpClient := http.Client{
		Timeout: 30 * time.Second,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}
	api := artemis.NewAPI(httpClient, "https://localhost/artemis", apiKey, apiSecret)
	version, err := api.GetVersion()
	if err != nil {
		log.Fatal(err)
	}
	var msg = ""
	if version.Msg != nil {
		msg = *version.Msg
	}
	log.Infof("Code: %s, msg: %s", version.Code, msg)
	var softVersion, productName = "", ""
	if version.Data != nil {
		if version.Data.SoftVersion != nil {
			softVersion = *version.Data.SoftVersion
		}
		if version.Data.ProductName != nil {
			productName = *version.Data.ProductName
		}
	}
	log.Infof("artemisAPI version: %s, produceName: %s", softVersion, productName)
	//types := []artemis.EventTypeCode{artemis.EventAccessGrantedByFace}
	//subscribeResponse, err := api.SubscribeByEventTypes(artemis.SubscribeByEventTypes{
	//	EventTypes: types,
	//	EventDest:  "http://localhost:9999/subscribe",
	//})
	//if subscribeResponse.Code == "0" {
	//	log.Info("Subscribed successfully")
	//}
}
