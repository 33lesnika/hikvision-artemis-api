package artemis

type EventMessage struct {
	Method string     `json:"method"`
	Params EventParam `json:"params"`
}

type EventParam struct {
	SendTime string  `json:"sendTime"`
	Ability  string  `json:"ability"`
	Events   []Event `json:"events"`
}

type Event struct {
	EventId       string  `json:"eventId"`
	SrcIndex      string  `json:"srcIndex"`
	SrcType       string  `json:"srcType"`
	SrcName       *string `json:"srcName"`
	EventType     int     `json:"eventType"`
	Status        int     `json:"status"`
	EventLvl      *int    `json:"eventLvl"`
	Timeout       int     `json:"timeout"`
	HappenTime    string  `json:"happenTime"`
	SrcParentIdex *string `json:"srcParentIdex"`
	Data          *string `json:"data"`
}
