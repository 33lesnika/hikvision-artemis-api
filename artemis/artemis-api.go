package artemis

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/labstack/gommon/log"
	"io"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"
)

type API struct {
	Client              http.Client
	BaseUrl             string
	apiKey              string
	apiSecret           string
	RequestTransformers []RequestTransformerFn
}

func NewAPI(client http.Client, baseUrl string, apiKey string, apiSecret string) API {

	return API{
		Client:    client,
		BaseUrl:   baseUrl,
		apiKey:    apiKey,
		apiSecret: apiSecret,
		RequestTransformers: []RequestTransformerFn{
			addHeaderParamsTransformer,
			authRequestTransformer,
		},
	}
}

type RequestTransformerFn func(client *API, httpRequest *http.Request, apiRequest any) error

func authRequestTransformer(client *API, httpRequest *http.Request, apiRequest any) error {
	httpRequest.Header.Set("Accept", "application/json")
	httpRequest.Header.Set("Content-Type", "application/json;charset=UTF-8")
	httpRequest.Header.Set("x-ca-key", client.apiKey)
	nonce := uuid.NewString()
	httpRequest.Header.Set("x-ca-nonce", nonce)
	currentTimestamp := strconv.FormatInt(time.Now().UnixMilli(), 10)
	httpRequest.Header.Set("x-ca-timestamp", currentTimestamp)
	var sb strings.Builder
	sb.WriteString(httpRequest.Method + "\n")
	sb.WriteString(httpRequest.Header.Get("Accept") + "\n")
	sb.WriteString(httpRequest.Header.Get("Content-Type") + "\n")
	sb.WriteString("x-ca-key:" + client.apiKey + "\n")
	sb.WriteString("x-ca-nonce:" + nonce + "\n")
	sb.WriteString("x-ca-timestamp:" + currentTimestamp + "\n")
	path := httpRequest.URL.Path
	log.Info(path)
	sb.WriteString(path)
	hash := hmac.New(sha256.New, []byte(client.apiSecret))
	signatureString := sb.String()
	hash.Write([]byte(signatureString))
	signature := base64.StdEncoding.EncodeToString(hash.Sum(nil))
	httpRequest.Header.Add("x-ca-signature-headers", "x-ca-key,x-ca-nonce,x-ca-timestamp")
	httpRequest.Header.Add("x-ca-signature", signature)
	return nil
}

func addHeaderParamsTransformer(client *API, httpRequest *http.Request, apiRequest any) error {
	if apiRequest == nil {
		return nil
	}
	s := reflect.ValueOf(&apiRequest).Elem()
	typeOfRequest := s.Type()
	for i := 0; i < s.NumField(); i++ {
		field := s.Field(i)
		fieldType := typeOfRequest.Field(i)
		tag := fieldType.Tag.Get("paramType")
		if tag == "header" {
			httpRequest.Header.Add(fieldType.Name, fmt.Sprintf("%v", field.Interface()))
		}
	}
	return nil
}

type APIRequest struct {
}

type APIResponse struct {
	Code string  `json:"code"`
	Msg  *string `json:"msg"`
}

type VersionData struct {
	ProductName *string `json:"produceName"`
	SoftVersion *string `json:"softVersion"`
}

type VersionResponse struct {
	APIResponse
	Data *VersionData `json:"data"`
}

func (c API) GetVersion() (*VersionResponse, error) {
	response := &VersionResponse{}
	err := c.sendRequest(http.MethodPost, c.BaseUrl+"/api/common/v1/version", nil, &response)
	return response, err
}

type PersonInfo struct {
	PersonCode       *string       `json:"personCode"`
	PersonFamilyName string        `json:"personFamilyName"`
	PersonGivenName  string        `json:"personGivenName"`
	Gender           *Gender       `json:"gender"`
	OrgIndexCode     string        `json:"orgIndexCode"`
	Remark           *string       `json:"remark"`
	PhoneNo          *string       `json:"phoneNo"`
	Email            *string       `json:"email"`
	Faces            []FaceData    `json:"faces"`
	FingerPrints     []FingerPrint `json:"fingerPrint"`
	Cards            []Card        `json:"cards"`
	BeginTime        *string       `json:"beginTime"`
	EndTime          *string       `json:"endTime"`
}

type Gender int

type Card struct {
	CardNo string `json:"cardNo"`
}

type FaceData struct {
	FaceData string `json:"faceData"`
}

type FingerPrint struct {
	FingerPrintIndexCode *string `json:"fingerPrintIndexCode"`
	FingerPrintName      *string `json:"fingerPrintName"`
	FingerPrintData      string  `json:"fingerPrintData"`
	RelatedCardNo        *string `json:"relatedCardNo"`
}

type AddPersonRequest struct {
	UserID *string `json:"-" paramType:"header"`
	PersonInfo
}

type AddPersonResponse struct {
	APIResponse
	//Data ID of added person
	Data string `json:"data"`
}

func (c API) AddPerson(request AddPersonRequest) (*AddPersonResponse, error) {
	response := &AddPersonResponse{}
	err := c.sendRequest(http.MethodPost, c.BaseUrl+"/api/resource/v1/person/single/add", request, response)
	return response, err
}

type PersonInfoByCodeRequest struct {
	APIRequest
	UserId     string `json:"-" paramType:"header"`
	PersonCode string `json:"personCode"`
}

type PersonInfoResponse struct {
	APIResponse
	Data PersonInfo `json:"data"`
}

func (c API) GetPersonInfoByPersonCode(request PersonInfoByCodeRequest) (*PersonInfoResponse, error) {
	response := &PersonInfoResponse{}
	err := c.sendRequest(http.MethodPost, c.BaseUrl+"/artemis/api/resource/v1/person/personCode/personInfo", request, response)
	return response, err
}

type EventTypeCode int

const (
	EventFaceDetectionAlarm  = 1482753
	EventAccessGrantedByFace = 196893
)

type SubscribeByEventTypes struct {
	APIRequest
	EventTypes []EventTypeCode `json:"eventTypes"`
	EventDest  string          `json:"eventDest"`
	Token      *string         `json:"token"`
}

type CancelSubscriptionByEventTypes struct {
	APIRequest
	EventTypes []EventTypeCode `json:"eventTypes"`
}

func (c API) SubscribeByEventTypes(request SubscribeByEventTypes) (*APIResponse, error) {
	response := &APIResponse{}
	err := c.sendRequest(http.MethodPost, c.BaseUrl+"/api/eventService/v1/eventSubscriptionByEventTypes", request, response)
	return response, err
}

func (c API) CancelSubscriptionByEventTypes(request CancelSubscriptionByEventTypes) (*APIResponse, error) {
	response := &APIResponse{}
	err := c.sendRequest(http.MethodPost, c.BaseUrl+"/api/eventService/v1/eventUnSubscriptionByEventTypes", request, response)
	return response, err
}

type EventImageDataRequest struct {
	APIRequest
	UserId   *string `json:"-" paramType:"header"`
	DomainID *string `json:"-" paramType:"header"`
	PicUri   string  `json:"picUri"`
}

type EventImageDataResponse struct {
	APIResponse
	Data string
}

// GetEventImageData Get alarm pictures from the searched alarm or event information by picture URL
func (c API) GetEventImageData(request EventImageDataRequest) (*EventImageDataResponse, error) {
	response := &EventImageDataResponse{}
	err := c.sendRequest(http.MethodPost, c.BaseUrl+"/artemis/api/eventService/v1/image_data", request, response)
	return response, err
}

func (c API) sendRequest(method string, url string, request any, response any) error {
	requestBytes, err := json.Marshal(request)
	httpRequest, err := http.NewRequest(method, url, bytes.NewBuffer(requestBytes))
	if err != nil {
		return err
	}
	c.applyTransformers(httpRequest, request)
	resp, err := c.Client.Do(httpRequest)
	if err != nil {
		return err
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(resp.Body)
	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		return err
	}
	return nil
}

func (c API) applyTransformers(httpRequest *http.Request, request any) {
	for _, transformer := range c.RequestTransformers {
		err := transformer(&c, httpRequest, request)
		if err != nil {
			log.Warn("Error in httpRequest transformer: %s", err)
		}
	}
}
